#include "led_counter.h"

uint8_t led_counter_state = 0;

void Led_Counter_Init()
	{
		led_counter_state = 0;
	}

uint8_t Led_Counter_Get_Next()
	{
		if(++led_counter_state > 7)
			led_counter_state = 0;
		return led_counter_state;
	}
