#include "gtest/gtest.h"

extern "C" {
#include "led_counter.h"
}

// Test fixture
class LedCounterTest: public ::testing::Test
{
protected:
    void SetUp()
    {
        Led_Counter_Init();
    }
};

// Check initial value
TEST_F(LedCounterTest, TestInitialValue)
{
    Led_Counter_Init();
    ASSERT_EQ(Led_Counter_Get_Next(), 1);
}

// Check how value is incremented
TEST_F(LedCounterTest, TestIncrementValue)
{
    Led_Counter_Init();
    unsigned int val = Led_Counter_Get_Next();
    for(int i = 0; i < 1; i++)
    {
        ASSERT_EQ(Led_Counter_Get_Next(), ++val);
    }
}

// Check how value return to 0 after 7
TEST_F(LedCounterTest, TestZeroCrossing)
{
    Led_Counter_Init();
    for(int i = 0; i < 7; i++)
    {
        Led_Counter_Get_Next();
    }
    ASSERT_EQ(Led_Counter_Get_Next(), 0);
}




