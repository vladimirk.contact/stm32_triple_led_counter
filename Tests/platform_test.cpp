#include <gtest/gtest.h>

	TEST(PlatformTest, TestPointerSize)
		{
			//Check pointer size is 32 bit
			ASSERT_EQ(sizeof(void*)*8, 32U);
		}
