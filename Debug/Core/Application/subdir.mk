################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Application/main.c \
../Core/Application/stm32f7xx_hal_msp.c \
../Core/Application/stm32f7xx_it.c \
../Core/Application/syscalls.c \
../Core/Application/sysmem.c \
../Core/Application/system_stm32f7xx.c 

C_DEPS += \
./Core/Application/main.d \
./Core/Application/stm32f7xx_hal_msp.d \
./Core/Application/stm32f7xx_it.d \
./Core/Application/syscalls.d \
./Core/Application/sysmem.d \
./Core/Application/system_stm32f7xx.d 

OBJS += \
./Core/Application/main.o \
./Core/Application/stm32f7xx_hal_msp.o \
./Core/Application/stm32f7xx_it.o \
./Core/Application/syscalls.o \
./Core/Application/sysmem.o \
./Core/Application/system_stm32f7xx.o 


# Each subdirectory must supply rules for building sources it contributes
Core/Application/%.o: ../Core/Application/%.c Core/Application/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F767xx -c -I../Core/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

